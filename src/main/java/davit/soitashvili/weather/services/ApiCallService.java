package davit.soitashvili.weather.services;

public interface ApiCallService {
    String makeGetCall(String url) throws Exception;

}
