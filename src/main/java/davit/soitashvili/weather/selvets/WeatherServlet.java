package davit.soitashvili.weather.selvets;

import davit.soitashvili.weather.services.ApiCallService;
import davit.soitashvili.weather.services.ApiCallServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/weather")
public class WeatherServlet extends HttpServlet {

    public static final String VIEW = "/weather.jsp";
    private static final String WEATHER_API_BASE_URL = "http://localhost:8090/davit_soitashvili_davaleba_1_war_exploded/api/weather/";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view = req.getRequestDispatcher(VIEW);
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userChoice = req.getParameter("userChoice");
        RequestDispatcher view = req.getRequestDispatcher(VIEW);
        try {
            req.setAttribute("result", getApiResult(userChoice));

        } catch (Exception e) {
            req.setAttribute("result", "Please type correct value!");
            e.printStackTrace();
        }
        view.forward(req, resp);
    }

    private String getApiResult(String userChoice) throws Exception {
        ApiCallService apiCallService = new ApiCallServiceImpl();
        if (userChoice.equals("all")) {
            return apiCallService.makeGetCall(WEATHER_API_BASE_URL);
        }
        return apiCallService.makeGetCall(WEATHER_API_BASE_URL + userChoice);
    }
}
