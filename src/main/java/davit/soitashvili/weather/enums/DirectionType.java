package davit.soitashvili.weather.enums;

public enum DirectionType {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
